﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;



[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="PretestNisrina")]
public partial class DataClassesDatabaseDataContext : System.Data.Linq.DataContext
{
	
	private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
	
  #region Extensibility Method Definitions
  partial void OnCreated();
  partial void InsertTBCompany(TBCompany instance);
  partial void UpdateTBCompany(TBCompany instance);
  partial void DeleteTBCompany(TBCompany instance);
  partial void InsertTBDocument(TBDocument instance);
  partial void UpdateTBDocument(TBDocument instance);
  partial void DeleteTBDocument(TBDocument instance);
  partial void InsertTBDocumentCategory(TBDocumentCategory instance);
  partial void UpdateTBDocumentCategory(TBDocumentCategory instance);
  partial void DeleteTBDocumentCategory(TBDocumentCategory instance);
  partial void InsertTBPosition(TBPosition instance);
  partial void UpdateTBPosition(TBPosition instance);
  partial void DeleteTBPosition(TBPosition instance);
  partial void InsertTBUser(TBUser instance);
  partial void UpdateTBUser(TBUser instance);
  partial void DeleteTBUser(TBUser instance);
    #endregion

    public DataClassesDatabaseDataContext() :
            base(global::System.Configuration.ConfigurationManager.ConnectionStrings["PretestNisrinaConnectionString"].ConnectionString, mappingSource)
    {
        OnCreated();
    }

    public DataClassesDatabaseDataContext(System.Data.IDbConnection connection) : 
			base(connection, mappingSource)
	{
		OnCreated();
	}
	
	public DataClassesDatabaseDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
			base(connection, mappingSource)
	{
		OnCreated();
	}
	
	public DataClassesDatabaseDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
			base(connection, mappingSource)
	{
		OnCreated();
	}
	
	public System.Data.Linq.Table<TBCompany> TBCompanies
	{
		get
		{
			return this.GetTable<TBCompany>();
		}
	}
	
	public System.Data.Linq.Table<TBDocument> TBDocuments
	{
		get
		{
			return this.GetTable<TBDocument>();
		}
	}
	
	public System.Data.Linq.Table<TBDocumentCategory> TBDocumentCategories
	{
		get
		{
			return this.GetTable<TBDocumentCategory>();
		}
	}
	
	public System.Data.Linq.Table<TBPosition> TBPositions
	{
		get
		{
			return this.GetTable<TBPosition>();
		}
	}
	
	public System.Data.Linq.Table<TBUser> TBUsers
	{
		get
		{
			return this.GetTable<TBUser>();
		}
	}
}

[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.TBCompany")]
public partial class TBCompany : INotifyPropertyChanging, INotifyPropertyChanged
{
	
	private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
	
	private int _ID;
	
	private System.Nullable<System.Guid> _UID;
	
	private string _Name;
	
	private string _Address;
	
	private string _Email;
	
	private string _Telephone;
	
	private System.Nullable<int> _Flag;
	
	private System.Nullable<int> _CreatedBy;
	
	private System.Nullable<System.DateTime> _CreatedAt;
	
	private EntitySet<TBDocument> _TBDocuments;
	
	private EntitySet<TBUser> _TBUsers;
	
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDChanging(int value);
    partial void OnIDChanged();
    partial void OnUIDChanging(System.Nullable<System.Guid> value);
    partial void OnUIDChanged();
    partial void OnNameChanging(string value);
    partial void OnNameChanged();
    partial void OnAddressChanging(string value);
    partial void OnAddressChanged();
    partial void OnEmailChanging(string value);
    partial void OnEmailChanged();
    partial void OnTelephoneChanging(string value);
    partial void OnTelephoneChanged();
    partial void OnFlagChanging(System.Nullable<int> value);
    partial void OnFlagChanged();
    partial void OnCreatedByChanging(System.Nullable<int> value);
    partial void OnCreatedByChanged();
    partial void OnCreatedAtChanging(System.Nullable<System.DateTime> value);
    partial void OnCreatedAtChanged();
    #endregion
	
	public TBCompany()
	{
		this._TBDocuments = new EntitySet<TBDocument>(new Action<TBDocument>(this.attach_TBDocuments), new Action<TBDocument>(this.detach_TBDocuments));
		this._TBUsers = new EntitySet<TBUser>(new Action<TBUser>(this.attach_TBUsers), new Action<TBUser>(this.detach_TBUsers));
		OnCreated();
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ID", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
	public int ID
	{
		get
		{
			return this._ID;
		}
		set
		{
			if ((this._ID != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._ID = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_UID", DbType="UniqueIdentifier")]
	public System.Nullable<System.Guid> UID
	{
		get
		{
			return this._UID;
		}
		set
		{
			if ((this._UID != value))
			{
				this.OnUIDChanging(value);
				this.SendPropertyChanging();
				this._UID = value;
				this.SendPropertyChanged("UID");
				this.OnUIDChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Name", DbType="VarChar(255)")]
	public string Name
	{
		get
		{
			return this._Name;
		}
		set
		{
			if ((this._Name != value))
			{
				this.OnNameChanging(value);
				this.SendPropertyChanging();
				this._Name = value;
				this.SendPropertyChanged("Name");
				this.OnNameChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Address", DbType="Text", UpdateCheck=UpdateCheck.Never)]
	public string Address
	{
		get
		{
			return this._Address;
		}
		set
		{
			if ((this._Address != value))
			{
				this.OnAddressChanging(value);
				this.SendPropertyChanging();
				this._Address = value;
				this.SendPropertyChanged("Address");
				this.OnAddressChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Email", DbType="VarChar(50)")]
	public string Email
	{
		get
		{
			return this._Email;
		}
		set
		{
			if ((this._Email != value))
			{
				this.OnEmailChanging(value);
				this.SendPropertyChanging();
				this._Email = value;
				this.SendPropertyChanged("Email");
				this.OnEmailChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Telephone", DbType="VarChar(14)")]
	public string Telephone
	{
		get
		{
			return this._Telephone;
		}
		set
		{
			if ((this._Telephone != value))
			{
				this.OnTelephoneChanging(value);
				this.SendPropertyChanging();
				this._Telephone = value;
				this.SendPropertyChanged("Telephone");
				this.OnTelephoneChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Flag", DbType="Int")]
	public System.Nullable<int> Flag
	{
		get
		{
			return this._Flag;
		}
		set
		{
			if ((this._Flag != value))
			{
				this.OnFlagChanging(value);
				this.SendPropertyChanging();
				this._Flag = value;
				this.SendPropertyChanged("Flag");
				this.OnFlagChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CreatedBy", DbType="Int")]
	public System.Nullable<int> CreatedBy
	{
		get
		{
			return this._CreatedBy;
		}
		set
		{
			if ((this._CreatedBy != value))
			{
				this.OnCreatedByChanging(value);
				this.SendPropertyChanging();
				this._CreatedBy = value;
				this.SendPropertyChanged("CreatedBy");
				this.OnCreatedByChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CreatedAt", DbType="DateTime")]
	public System.Nullable<System.DateTime> CreatedAt
	{
		get
		{
			return this._CreatedAt;
		}
		set
		{
			if ((this._CreatedAt != value))
			{
				this.OnCreatedAtChanging(value);
				this.SendPropertyChanging();
				this._CreatedAt = value;
				this.SendPropertyChanged("CreatedAt");
				this.OnCreatedAtChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.AssociationAttribute(Name="TBCompany_TBDocument", Storage="_TBDocuments", ThisKey="ID", OtherKey="IDCompany")]
	public EntitySet<TBDocument> TBDocuments
	{
		get
		{
			return this._TBDocuments;
		}
		set
		{
			this._TBDocuments.Assign(value);
		}
	}
	
	[global::System.Data.Linq.Mapping.AssociationAttribute(Name="TBCompany_TBUser", Storage="_TBUsers", ThisKey="ID", OtherKey="IDCompany")]
	public EntitySet<TBUser> TBUsers
	{
		get
		{
			return this._TBUsers;
		}
		set
		{
			this._TBUsers.Assign(value);
		}
	}
	
	public event PropertyChangingEventHandler PropertyChanging;
	
	public event PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		if ((this.PropertyChanging != null))
		{
			this.PropertyChanging(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(String propertyName)
	{
		if ((this.PropertyChanged != null))
		{
			this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}
	
	private void attach_TBDocuments(TBDocument entity)
	{
		this.SendPropertyChanging();
		entity.TBCompany = this;
	}
	
	private void detach_TBDocuments(TBDocument entity)
	{
		this.SendPropertyChanging();
		entity.TBCompany = null;
	}
	
	private void attach_TBUsers(TBUser entity)
	{
		this.SendPropertyChanging();
		entity.TBCompany = this;
	}
	
	private void detach_TBUsers(TBUser entity)
	{
		this.SendPropertyChanging();
		entity.TBCompany = null;
	}
}

[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.TBDocument")]
public partial class TBDocument : INotifyPropertyChanging, INotifyPropertyChanged
{
	
	private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
	
	private int _ID;
	
	private System.Nullable<System.Guid> _UID;
	
	private System.Nullable<int> _IDCompany;
	
	private System.Nullable<int> _IDCategory;
	
	private string _Name;
	
	private string _Description;
	
	private System.Nullable<int> _Flag;
	
	private System.Nullable<int> _CreatedBy;
	
	private System.Nullable<System.DateTime> _CreatedAt;
	
	private EntityRef<TBCompany> _TBCompany;
	
	private EntityRef<TBDocumentCategory> _TBDocumentCategory;
	
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDChanging(int value);
    partial void OnIDChanged();
    partial void OnUIDChanging(System.Nullable<System.Guid> value);
    partial void OnUIDChanged();
    partial void OnIDCompanyChanging(System.Nullable<int> value);
    partial void OnIDCompanyChanged();
    partial void OnIDCategoryChanging(System.Nullable<int> value);
    partial void OnIDCategoryChanged();
    partial void OnNameChanging(string value);
    partial void OnNameChanged();
    partial void OnDescriptionChanging(string value);
    partial void OnDescriptionChanged();
    partial void OnFlagChanging(System.Nullable<int> value);
    partial void OnFlagChanged();
    partial void OnCreatedByChanging(System.Nullable<int> value);
    partial void OnCreatedByChanged();
    partial void OnCreatedAtChanging(System.Nullable<System.DateTime> value);
    partial void OnCreatedAtChanged();
    #endregion
	
	public TBDocument()
	{
		this._TBCompany = default(EntityRef<TBCompany>);
		this._TBDocumentCategory = default(EntityRef<TBDocumentCategory>);
		OnCreated();
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ID", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
	public int ID
	{
		get
		{
			return this._ID;
		}
		set
		{
			if ((this._ID != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._ID = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_UID", DbType="UniqueIdentifier")]
	public System.Nullable<System.Guid> UID
	{
		get
		{
			return this._UID;
		}
		set
		{
			if ((this._UID != value))
			{
				this.OnUIDChanging(value);
				this.SendPropertyChanging();
				this._UID = value;
				this.SendPropertyChanged("UID");
				this.OnUIDChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_IDCompany", DbType="Int")]
	public System.Nullable<int> IDCompany
	{
		get
		{
			return this._IDCompany;
		}
		set
		{
			if ((this._IDCompany != value))
			{
				if (this._TBCompany.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnIDCompanyChanging(value);
				this.SendPropertyChanging();
				this._IDCompany = value;
				this.SendPropertyChanged("IDCompany");
				this.OnIDCompanyChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_IDCategory", DbType="Int")]
	public System.Nullable<int> IDCategory
	{
		get
		{
			return this._IDCategory;
		}
		set
		{
			if ((this._IDCategory != value))
			{
				if (this._TBDocumentCategory.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnIDCategoryChanging(value);
				this.SendPropertyChanging();
				this._IDCategory = value;
				this.SendPropertyChanged("IDCategory");
				this.OnIDCategoryChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Name", DbType="VarChar(255)")]
	public string Name
	{
		get
		{
			return this._Name;
		}
		set
		{
			if ((this._Name != value))
			{
				this.OnNameChanging(value);
				this.SendPropertyChanging();
				this._Name = value;
				this.SendPropertyChanged("Name");
				this.OnNameChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Description", DbType="Text", UpdateCheck=UpdateCheck.Never)]
	public string Description
	{
		get
		{
			return this._Description;
		}
		set
		{
			if ((this._Description != value))
			{
				this.OnDescriptionChanging(value);
				this.SendPropertyChanging();
				this._Description = value;
				this.SendPropertyChanged("Description");
				this.OnDescriptionChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Flag", DbType="Int")]
	public System.Nullable<int> Flag
	{
		get
		{
			return this._Flag;
		}
		set
		{
			if ((this._Flag != value))
			{
				this.OnFlagChanging(value);
				this.SendPropertyChanging();
				this._Flag = value;
				this.SendPropertyChanged("Flag");
				this.OnFlagChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CreatedBy", DbType="Int")]
	public System.Nullable<int> CreatedBy
	{
		get
		{
			return this._CreatedBy;
		}
		set
		{
			if ((this._CreatedBy != value))
			{
				this.OnCreatedByChanging(value);
				this.SendPropertyChanging();
				this._CreatedBy = value;
				this.SendPropertyChanged("CreatedBy");
				this.OnCreatedByChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CreatedAt", DbType="DateTime")]
	public System.Nullable<System.DateTime> CreatedAt
	{
		get
		{
			return this._CreatedAt;
		}
		set
		{
			if ((this._CreatedAt != value))
			{
				this.OnCreatedAtChanging(value);
				this.SendPropertyChanging();
				this._CreatedAt = value;
				this.SendPropertyChanged("CreatedAt");
				this.OnCreatedAtChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.AssociationAttribute(Name="TBCompany_TBDocument", Storage="_TBCompany", ThisKey="IDCompany", OtherKey="ID", IsForeignKey=true)]
	public TBCompany TBCompany
	{
		get
		{
			return this._TBCompany.Entity;
		}
		set
		{
			TBCompany previousValue = this._TBCompany.Entity;
			if (((previousValue != value) 
						|| (this._TBCompany.HasLoadedOrAssignedValue == false)))
			{
				this.SendPropertyChanging();
				if ((previousValue != null))
				{
					this._TBCompany.Entity = null;
					previousValue.TBDocuments.Remove(this);
				}
				this._TBCompany.Entity = value;
				if ((value != null))
				{
					value.TBDocuments.Add(this);
					this._IDCompany = value.ID;
				}
				else
				{
					this._IDCompany = default(Nullable<int>);
				}
				this.SendPropertyChanged("TBCompany");
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.AssociationAttribute(Name="TBDocumentCategory_TBDocument", Storage="_TBDocumentCategory", ThisKey="IDCategory", OtherKey="ID", IsForeignKey=true)]
	public TBDocumentCategory TBDocumentCategory
	{
		get
		{
			return this._TBDocumentCategory.Entity;
		}
		set
		{
			TBDocumentCategory previousValue = this._TBDocumentCategory.Entity;
			if (((previousValue != value) 
						|| (this._TBDocumentCategory.HasLoadedOrAssignedValue == false)))
			{
				this.SendPropertyChanging();
				if ((previousValue != null))
				{
					this._TBDocumentCategory.Entity = null;
					previousValue.TBDocuments.Remove(this);
				}
				this._TBDocumentCategory.Entity = value;
				if ((value != null))
				{
					value.TBDocuments.Add(this);
					this._IDCategory = value.ID;
				}
				else
				{
					this._IDCategory = default(Nullable<int>);
				}
				this.SendPropertyChanged("TBDocumentCategory");
			}
		}
	}
	
	public event PropertyChangingEventHandler PropertyChanging;
	
	public event PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		if ((this.PropertyChanging != null))
		{
			this.PropertyChanging(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(String propertyName)
	{
		if ((this.PropertyChanged != null))
		{
			this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}

[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.TBDocumentCategory")]
public partial class TBDocumentCategory : INotifyPropertyChanging, INotifyPropertyChanged
{
	
	private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
	
	private int _ID;
	
	private System.Nullable<System.Guid> _UID;
	
	private string _Name;
	
	private System.Nullable<int> _CreatedBy;
	
	private System.Nullable<System.DateTime> _CreatedAt;
	
	private EntitySet<TBDocument> _TBDocuments;
	
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDChanging(int value);
    partial void OnIDChanged();
    partial void OnUIDChanging(System.Nullable<System.Guid> value);
    partial void OnUIDChanged();
    partial void OnNameChanging(string value);
    partial void OnNameChanged();
    partial void OnCreatedByChanging(System.Nullable<int> value);
    partial void OnCreatedByChanged();
    partial void OnCreatedAtChanging(System.Nullable<System.DateTime> value);
    partial void OnCreatedAtChanged();
    #endregion
	
	public TBDocumentCategory()
	{
		this._TBDocuments = new EntitySet<TBDocument>(new Action<TBDocument>(this.attach_TBDocuments), new Action<TBDocument>(this.detach_TBDocuments));
		OnCreated();
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ID", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
	public int ID
	{
		get
		{
			return this._ID;
		}
		set
		{
			if ((this._ID != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._ID = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_UID", DbType="UniqueIdentifier")]
	public System.Nullable<System.Guid> UID
	{
		get
		{
			return this._UID;
		}
		set
		{
			if ((this._UID != value))
			{
				this.OnUIDChanging(value);
				this.SendPropertyChanging();
				this._UID = value;
				this.SendPropertyChanged("UID");
				this.OnUIDChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Name", DbType="VarChar(255)")]
	public string Name
	{
		get
		{
			return this._Name;
		}
		set
		{
			if ((this._Name != value))
			{
				this.OnNameChanging(value);
				this.SendPropertyChanging();
				this._Name = value;
				this.SendPropertyChanged("Name");
				this.OnNameChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CreatedBy", DbType="Int")]
	public System.Nullable<int> CreatedBy
	{
		get
		{
			return this._CreatedBy;
		}
		set
		{
			if ((this._CreatedBy != value))
			{
				this.OnCreatedByChanging(value);
				this.SendPropertyChanging();
				this._CreatedBy = value;
				this.SendPropertyChanged("CreatedBy");
				this.OnCreatedByChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CreatedAt", DbType="DateTime")]
	public System.Nullable<System.DateTime> CreatedAt
	{
		get
		{
			return this._CreatedAt;
		}
		set
		{
			if ((this._CreatedAt != value))
			{
				this.OnCreatedAtChanging(value);
				this.SendPropertyChanging();
				this._CreatedAt = value;
				this.SendPropertyChanged("CreatedAt");
				this.OnCreatedAtChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.AssociationAttribute(Name="TBDocumentCategory_TBDocument", Storage="_TBDocuments", ThisKey="ID", OtherKey="IDCategory")]
	public EntitySet<TBDocument> TBDocuments
	{
		get
		{
			return this._TBDocuments;
		}
		set
		{
			this._TBDocuments.Assign(value);
		}
	}
	
	public event PropertyChangingEventHandler PropertyChanging;
	
	public event PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		if ((this.PropertyChanging != null))
		{
			this.PropertyChanging(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(String propertyName)
	{
		if ((this.PropertyChanged != null))
		{
			this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}
	
	private void attach_TBDocuments(TBDocument entity)
	{
		this.SendPropertyChanging();
		entity.TBDocumentCategory = this;
	}
	
	private void detach_TBDocuments(TBDocument entity)
	{
		this.SendPropertyChanging();
		entity.TBDocumentCategory = null;
	}
}

[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.TBPosition")]
public partial class TBPosition : INotifyPropertyChanging, INotifyPropertyChanged
{
	
	private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
	
	private int _ID;
	
	private System.Nullable<System.Guid> _UID;
	
	private string _Name;
	
	private System.Nullable<int> _CreatedBy;
	
	private System.Nullable<System.DateTime> _CreatedAt;
	
	private EntitySet<TBUser> _TBUsers;
	
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDChanging(int value);
    partial void OnIDChanged();
    partial void OnUIDChanging(System.Nullable<System.Guid> value);
    partial void OnUIDChanged();
    partial void OnNameChanging(string value);
    partial void OnNameChanged();
    partial void OnCreatedByChanging(System.Nullable<int> value);
    partial void OnCreatedByChanged();
    partial void OnCreatedAtChanging(System.Nullable<System.DateTime> value);
    partial void OnCreatedAtChanged();
    #endregion
	
	public TBPosition()
	{
		this._TBUsers = new EntitySet<TBUser>(new Action<TBUser>(this.attach_TBUsers), new Action<TBUser>(this.detach_TBUsers));
		OnCreated();
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ID", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
	public int ID
	{
		get
		{
			return this._ID;
		}
		set
		{
			if ((this._ID != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._ID = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_UID", DbType="UniqueIdentifier")]
	public System.Nullable<System.Guid> UID
	{
		get
		{
			return this._UID;
		}
		set
		{
			if ((this._UID != value))
			{
				this.OnUIDChanging(value);
				this.SendPropertyChanging();
				this._UID = value;
				this.SendPropertyChanged("UID");
				this.OnUIDChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Name", DbType="VarChar(255)")]
	public string Name
	{
		get
		{
			return this._Name;
		}
		set
		{
			if ((this._Name != value))
			{
				this.OnNameChanging(value);
				this.SendPropertyChanging();
				this._Name = value;
				this.SendPropertyChanged("Name");
				this.OnNameChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CreatedBy", DbType="Int")]
	public System.Nullable<int> CreatedBy
	{
		get
		{
			return this._CreatedBy;
		}
		set
		{
			if ((this._CreatedBy != value))
			{
				this.OnCreatedByChanging(value);
				this.SendPropertyChanging();
				this._CreatedBy = value;
				this.SendPropertyChanged("CreatedBy");
				this.OnCreatedByChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CreatedAt", DbType="DateTime")]
	public System.Nullable<System.DateTime> CreatedAt
	{
		get
		{
			return this._CreatedAt;
		}
		set
		{
			if ((this._CreatedAt != value))
			{
				this.OnCreatedAtChanging(value);
				this.SendPropertyChanging();
				this._CreatedAt = value;
				this.SendPropertyChanged("CreatedAt");
				this.OnCreatedAtChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.AssociationAttribute(Name="TBPosition_TBUser", Storage="_TBUsers", ThisKey="ID", OtherKey="IDPosition")]
	public EntitySet<TBUser> TBUsers
	{
		get
		{
			return this._TBUsers;
		}
		set
		{
			this._TBUsers.Assign(value);
		}
	}
	
	public event PropertyChangingEventHandler PropertyChanging;
	
	public event PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		if ((this.PropertyChanging != null))
		{
			this.PropertyChanging(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(String propertyName)
	{
		if ((this.PropertyChanged != null))
		{
			this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}
	
	private void attach_TBUsers(TBUser entity)
	{
		this.SendPropertyChanging();
		entity.TBPosition = this;
	}
	
	private void detach_TBUsers(TBUser entity)
	{
		this.SendPropertyChanging();
		entity.TBPosition = null;
	}
}

[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.TBUser")]
public partial class TBUser : INotifyPropertyChanging, INotifyPropertyChanged
{
	
	private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
	
	private int _ID;
	
	private System.Nullable<System.Guid> _UID;
	
	private System.Nullable<int> _IDCompany;
	
	private System.Nullable<int> _IDPosition;
	
	private string _Name;
	
	private string _Address;
	
	private string _Email;
	
	private string _Telephone;
	
	private string _Username;
	
	private string _Password;
	
	private string _Role;
	
	private System.Nullable<int> _Flag;
	
	private System.Nullable<int> _CreatedBy;
	
	private System.Nullable<System.DateTime> _CreatedAt;
	
	private EntityRef<TBCompany> _TBCompany;
	
	private EntityRef<TBPosition> _TBPosition;
	
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDChanging(int value);
    partial void OnIDChanged();
    partial void OnUIDChanging(System.Nullable<System.Guid> value);
    partial void OnUIDChanged();
    partial void OnIDCompanyChanging(System.Nullable<int> value);
    partial void OnIDCompanyChanged();
    partial void OnIDPositionChanging(System.Nullable<int> value);
    partial void OnIDPositionChanged();
    partial void OnNameChanging(string value);
    partial void OnNameChanged();
    partial void OnAddressChanging(string value);
    partial void OnAddressChanged();
    partial void OnEmailChanging(string value);
    partial void OnEmailChanged();
    partial void OnTelephoneChanging(string value);
    partial void OnTelephoneChanged();
    partial void OnUsernameChanging(string value);
    partial void OnUsernameChanged();
    partial void OnPasswordChanging(string value);
    partial void OnPasswordChanged();
    partial void OnRoleChanging(string value);
    partial void OnRoleChanged();
    partial void OnFlagChanging(System.Nullable<int> value);
    partial void OnFlagChanged();
    partial void OnCreatedByChanging(System.Nullable<int> value);
    partial void OnCreatedByChanged();
    partial void OnCreatedAtChanging(System.Nullable<System.DateTime> value);
    partial void OnCreatedAtChanged();
    #endregion
	
	public TBUser()
	{
		this._TBCompany = default(EntityRef<TBCompany>);
		this._TBPosition = default(EntityRef<TBPosition>);
		OnCreated();
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ID", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
	public int ID
	{
		get
		{
			return this._ID;
		}
		set
		{
			if ((this._ID != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._ID = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_UID", DbType="UniqueIdentifier")]
	public System.Nullable<System.Guid> UID
	{
		get
		{
			return this._UID;
		}
		set
		{
			if ((this._UID != value))
			{
				this.OnUIDChanging(value);
				this.SendPropertyChanging();
				this._UID = value;
				this.SendPropertyChanged("UID");
				this.OnUIDChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_IDCompany", DbType="Int")]
	public System.Nullable<int> IDCompany
	{
		get
		{
			return this._IDCompany;
		}
		set
		{
			if ((this._IDCompany != value))
			{
				if (this._TBCompany.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnIDCompanyChanging(value);
				this.SendPropertyChanging();
				this._IDCompany = value;
				this.SendPropertyChanged("IDCompany");
				this.OnIDCompanyChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_IDPosition", DbType="Int")]
	public System.Nullable<int> IDPosition
	{
		get
		{
			return this._IDPosition;
		}
		set
		{
			if ((this._IDPosition != value))
			{
				if (this._TBPosition.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnIDPositionChanging(value);
				this.SendPropertyChanging();
				this._IDPosition = value;
				this.SendPropertyChanged("IDPosition");
				this.OnIDPositionChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Name", DbType="VarChar(255)")]
	public string Name
	{
		get
		{
			return this._Name;
		}
		set
		{
			if ((this._Name != value))
			{
				this.OnNameChanging(value);
				this.SendPropertyChanging();
				this._Name = value;
				this.SendPropertyChanged("Name");
				this.OnNameChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Address", DbType="Text", UpdateCheck=UpdateCheck.Never)]
	public string Address
	{
		get
		{
			return this._Address;
		}
		set
		{
			if ((this._Address != value))
			{
				this.OnAddressChanging(value);
				this.SendPropertyChanging();
				this._Address = value;
				this.SendPropertyChanged("Address");
				this.OnAddressChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Email", DbType="VarChar(50)")]
	public string Email
	{
		get
		{
			return this._Email;
		}
		set
		{
			if ((this._Email != value))
			{
				this.OnEmailChanging(value);
				this.SendPropertyChanging();
				this._Email = value;
				this.SendPropertyChanged("Email");
				this.OnEmailChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Telephone", DbType="VarChar(14)")]
	public string Telephone
	{
		get
		{
			return this._Telephone;
		}
		set
		{
			if ((this._Telephone != value))
			{
				this.OnTelephoneChanging(value);
				this.SendPropertyChanging();
				this._Telephone = value;
				this.SendPropertyChanged("Telephone");
				this.OnTelephoneChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Username", DbType="VarChar(50)")]
	public string Username
	{
		get
		{
			return this._Username;
		}
		set
		{
			if ((this._Username != value))
			{
				this.OnUsernameChanging(value);
				this.SendPropertyChanging();
				this._Username = value;
				this.SendPropertyChanged("Username");
				this.OnUsernameChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Password", DbType="VarChar(50)")]
	public string Password
	{
		get
		{
			return this._Password;
		}
		set
		{
			if ((this._Password != value))
			{
				this.OnPasswordChanging(value);
				this.SendPropertyChanging();
				this._Password = value;
				this.SendPropertyChanged("Password");
				this.OnPasswordChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Role", DbType="VarChar(50)")]
	public string Role
	{
		get
		{
			return this._Role;
		}
		set
		{
			if ((this._Role != value))
			{
				this.OnRoleChanging(value);
				this.SendPropertyChanging();
				this._Role = value;
				this.SendPropertyChanged("Role");
				this.OnRoleChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Flag", DbType="Int")]
	public System.Nullable<int> Flag
	{
		get
		{
			return this._Flag;
		}
		set
		{
			if ((this._Flag != value))
			{
				this.OnFlagChanging(value);
				this.SendPropertyChanging();
				this._Flag = value;
				this.SendPropertyChanged("Flag");
				this.OnFlagChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CreatedBy", DbType="Int")]
	public System.Nullable<int> CreatedBy
	{
		get
		{
			return this._CreatedBy;
		}
		set
		{
			if ((this._CreatedBy != value))
			{
				this.OnCreatedByChanging(value);
				this.SendPropertyChanging();
				this._CreatedBy = value;
				this.SendPropertyChanged("CreatedBy");
				this.OnCreatedByChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CreatedAt", DbType="DateTime")]
	public System.Nullable<System.DateTime> CreatedAt
	{
		get
		{
			return this._CreatedAt;
		}
		set
		{
			if ((this._CreatedAt != value))
			{
				this.OnCreatedAtChanging(value);
				this.SendPropertyChanging();
				this._CreatedAt = value;
				this.SendPropertyChanged("CreatedAt");
				this.OnCreatedAtChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.AssociationAttribute(Name="TBCompany_TBUser", Storage="_TBCompany", ThisKey="IDCompany", OtherKey="ID", IsForeignKey=true)]
	public TBCompany TBCompany
	{
		get
		{
			return this._TBCompany.Entity;
		}
		set
		{
			TBCompany previousValue = this._TBCompany.Entity;
			if (((previousValue != value) 
						|| (this._TBCompany.HasLoadedOrAssignedValue == false)))
			{
				this.SendPropertyChanging();
				if ((previousValue != null))
				{
					this._TBCompany.Entity = null;
					previousValue.TBUsers.Remove(this);
				}
				this._TBCompany.Entity = value;
				if ((value != null))
				{
					value.TBUsers.Add(this);
					this._IDCompany = value.ID;
				}
				else
				{
					this._IDCompany = default(Nullable<int>);
				}
				this.SendPropertyChanged("TBCompany");
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.AssociationAttribute(Name="TBPosition_TBUser", Storage="_TBPosition", ThisKey="IDPosition", OtherKey="ID", IsForeignKey=true)]
	public TBPosition TBPosition
	{
		get
		{
			return this._TBPosition.Entity;
		}
		set
		{
			TBPosition previousValue = this._TBPosition.Entity;
			if (((previousValue != value) 
						|| (this._TBPosition.HasLoadedOrAssignedValue == false)))
			{
				this.SendPropertyChanging();
				if ((previousValue != null))
				{
					this._TBPosition.Entity = null;
					previousValue.TBUsers.Remove(this);
				}
				this._TBPosition.Entity = value;
				if ((value != null))
				{
					value.TBUsers.Add(this);
					this._IDPosition = value.ID;
				}
				else
				{
					this._IDPosition = default(Nullable<int>);
				}
				this.SendPropertyChanged("TBPosition");
			}
		}
	}
	
	public event PropertyChangingEventHandler PropertyChanging;
	
	public event PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		if ((this.PropertyChanging != null))
		{
			this.PropertyChanging(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(String propertyName)
	{
		if ((this.PropertyChanged != null))
		{
			this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
#pragma warning restore 1591
