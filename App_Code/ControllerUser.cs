﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.PeerToPeer;
using System.ServiceModel.Channels;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ControllerUser
/// </summary>
public class ControllerUser : ClassBase
{
    public ControllerUser(DataClassesDatabaseDataContext _db) : base(_db)
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public void ViewData()
    {
        db.TBUsers.Select(x => new
        {
            x.ID,
            x.IDCompany,
            NameCompany = x.TBCompany.Name,
            x.IDPosition,
            NamePosition = x.TBPosition.Name,
            x.Address,
            x.Email,
            x.Telephone,
            x.Username,
            x.Password,
            x.Role,
            x.Flag,
            x.CreatedBy
        }).ToArray();
    }

    public TBUser Create(int IDCompany, int IDPosition, string name, string address, string email, string telephone,string username,string password, string role)
    {
        TBUser user = new TBUser
        {
            UID = Guid.NewGuid(),
            IDCompany = IDCompany,
            IDPosition = IDPosition,
            Name = name,
            Address = address,
            Email = email,
            Telephone = telephone,
            Username = username,
            Password = password,
            Role = role,
            Flag= 1,
            CreatedBy = 1,
            CreatedAt = DateTime.Now
        };

        db.TBUsers.InsertOnSubmit(user);

        return user;
    }

    public TBUser Cari(int ID)
    {
        return db.TBUsers.FirstOrDefault(x => x.ID == ID);
    }

    public TBUser Update(int ID, int IDCompany, int IDPosition,string name, string address, string email, string telephone, string username, string password, string role)
    {
        var user = Cari(ID);

        if (user != null)
        {
            user.IDCompany = IDCompany;
            user.IDPosition = IDPosition;
            user.Name = name;
            user.Address = address;
            user.Email = email;
            user.Telephone = telephone;
            user.Username = username;
            user.Password = password;
            user.Role = role;
            user.Flag = 1;
            user.CreatedBy = 1;
            return user;
        }
        else
            return null;
    }
}