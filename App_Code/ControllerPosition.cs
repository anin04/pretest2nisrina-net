﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel.Channels;
using System.Threading;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ControllerBrand
/// </summary>
public class ControllerPosition : ClassBase
{
    public ControllerPosition(DataClassesDatabaseDataContext _db) : base(_db)
    {
        //
        // TODO: Add constructor logic here
        //
    }

    //Untuk menampilkan data & get data
    public TBPosition[] Data()
    {
        return db.TBPositions.ToArray();
    }

    //Create data
    public TBPosition Create(string name)
    {
        TBPosition Position = new TBPosition
        {
            Name = name,
            CreatedBy = 1,
            UID = Guid.NewGuid(),
            CreatedAt = DateTime.Now
        };

        db.TBPositions.InsertOnSubmit(Position);

        return Position;
    }

    //Search data
    public TBPosition Cari(string UID)
    {
        return db.TBPositions.FirstOrDefault(x => x.UID.ToString() == UID);
    }

    //Update
    public TBPosition Update(string UID, string name)
    {
        var position = Cari(UID);

        if (position != null)
        {
            position.Name = name;
            position.CreatedBy = 1;


            return position;
        }
        else
            return null;
    }

    //Delete
    public TBPosition Delete(string UID)
    {
        var position = Cari(UID);

        if (position != null)
        {
            db.TBPositions.DeleteOnSubmit(position);
            db.SubmitChanges();

            return position;
        }
        else
            return null;
    }
    public void DropDownListPosition(DropDownList dropDownList)
    {
        dropDownList.DataSource = Data();
        dropDownList.DataValueField = "ID";
        dropDownList.DataTextField = "Name";
        dropDownList.DataBind();

        dropDownList.Items.Insert(0, new ListItem { Value = "0", Text = "-Pilih-" });
    }

    public ListItem[] DropDownList()
    {
        List<ListItem> position = new List<ListItem>();

        position.Add(new ListItem { Value = "0", Text = "-Pilih-" });

        position.AddRange(Data().Select(x => new ListItem
        {
            Value = x.ID.ToString(),
            Text = x.Name
        }));

        return position.ToArray();
    }

}