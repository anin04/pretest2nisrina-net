﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ControllerCategory
/// </summary>
public class ControllerDocumentCategory : ClassBase
{
    public ControllerDocumentCategory(DataClassesDatabaseDataContext _db) : base(_db)
    {
        //
        // TODO: Add constructor logic here
        //
    }

    //Untuk menampilkan data & get data
    public TBDocumentCategory[] Data()
    {
        return db.TBDocumentCategories.ToArray();
    }

    //Create data
    public TBDocumentCategory Create(string name)
    {
        TBDocumentCategory DocumentCategory = new TBDocumentCategory
        {
            Name = name,
            CreatedBy = 1,
            UID = Guid.NewGuid(),
            CreatedAt = DateTime.Now
        };

        db.TBDocumentCategories.InsertOnSubmit(DocumentCategory);

        return DocumentCategory;
    }

    //Search data
    public TBDocumentCategory Cari(string UID)
    {
        return db.TBDocumentCategories.FirstOrDefault(x => x.UID.ToString() == UID);
    }

    //Update
    public TBDocumentCategory Update(string UID, string name)
    {
        var DocumentCategory = Cari(UID);

        if (DocumentCategory != null)
        {
            DocumentCategory.Name = name;
            DocumentCategory.CreatedBy = 1;


            return DocumentCategory;
        }
        else
            return null;
    }

    //Delete
    public TBDocumentCategory Delete(string UID)
    {
        var DocumentCategory = Cari(UID);

        if (DocumentCategory != null)
        {
            db.TBDocumentCategories.DeleteOnSubmit(DocumentCategory);
            db.SubmitChanges();

            return DocumentCategory;
        }
        else
            return null;
    }
    public void DropDownListDocumentCategory(DropDownList dropDownList)
    {
        dropDownList.DataSource = Data();
        dropDownList.DataValueField = "ID";
        dropDownList.DataTextField = "Name";
        dropDownList.DataBind();

        dropDownList.Items.Insert(0, new ListItem { Value = "0", Text = "-Pilih-" });
    }

    public ListItem[] DropDownList()
    {
        List<ListItem> DocumentCategory = new List<ListItem>();

        DocumentCategory.Add(new ListItem { Value = "0", Text = "-Pilih-" });

        DocumentCategory.AddRange(Data().Select(x => new ListItem
        {
            Value = x.ID.ToString(),
            Text = x.Name
        }));

        return DocumentCategory.ToArray();
    }
}