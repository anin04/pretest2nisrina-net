﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Position_Form : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerPosition controllerPosition = new ControllerPosition(db);

                var Position = controllerPosition.Cari(Request.QueryString["uid"]);

                if (Position != null)
                {
                    InputName.Text = Position.Name;

                    

                    ButtonOk.Text = "Update";
                    LabelTitle.Text = "Update Position";
                }
                else
                {
                    ButtonOk.Text = "Add New";
                    LabelTitle.Text = "Add New Position";
                }
            }
        }
    }

    protected void ButtonOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerPosition controllerPosition = new ControllerPosition(db);

                if (ButtonOk.Text == "Add New")
                    controllerPosition.Create(InputName.Text);
                else if (ButtonOk.Text == "Update")
                    controllerPosition.Update(Request.QueryString["uid"], InputName.Text);

                db.SubmitChanges();

                Response.Redirect("/Position/Default.aspx");
            }
        }
    }

    protected void ButtonKeluar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Position/Default.aspx");
    }
}