﻿using ImageResizer.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Position_Default : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                LoadData(db);
            }
        }
    }
   
    //Menampilkan data
    private void LoadData(DataClassesDatabaseDataContext db)
    {
        ControllerPosition controllerPosition = new ControllerPosition(db);

        repeaterPosition.DataSource = controllerPosition.Data();
        repeaterPosition.DataBind();
    }

    //Fungsi untuk update dan delete dalam table repeater
    protected void repeaterPosition_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
        {
            if (e.CommandName == "Update")
            {
                Response.Redirect("/Position/Form.aspx?uid=" + e.CommandArgument);
            }
            else if (e.CommandName == "Delete")
            {
                var position = db.TBPositions.FirstOrDefault(x => x.UID.ToString() == e.CommandArgument.ToString());
               
                db.TBPositions.DeleteOnSubmit(position);
                db.SubmitChanges();

                LoadData(db);
            }
        }
    }
}