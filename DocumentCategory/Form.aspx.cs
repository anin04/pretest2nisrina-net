﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DocumentCategory_Form : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerDocumentCategory controllerDocumentCategory = new ControllerDocumentCategory(db);

                var DocumentCategory = controllerDocumentCategory.Cari(Request.QueryString["uid"]);

                if (DocumentCategory != null)
                {
                    InputName.Text = DocumentCategory.Name;



                    ButtonOk.Text = "Update";
                    LabelTitle.Text = "Update Document Category";
                }
                else
                {
                    ButtonOk.Text = "Add New";
                    LabelTitle.Text = "Add New DocumentCategory";
                }
            }
        }
    }

    protected void ButtonOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerDocumentCategory controllerDocumentCategory = new ControllerDocumentCategory(db);

                if (ButtonOk.Text == "Add New")
                    controllerDocumentCategory.Create(InputName.Text);
                else if (ButtonOk.Text == "Update")
                    controllerDocumentCategory.Update(Request.QueryString["uid"], InputName.Text);

                db.SubmitChanges();

                Response.Redirect("/DocumentCategory/Default.aspx");
            }
        }
    }

    protected void ButtonKeluar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/DocumentCategory/Default.aspx");
    }
}