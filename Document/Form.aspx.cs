﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Document_Form : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerCompany controllerCompany = new ControllerCompany(db);
                listCompany.Items.AddRange(controllerCompany.DropDownList());

                ControllerDocumentCategory controllerDocumentCategory = new ControllerDocumentCategory(db);
                listCategory.Items.AddRange(controllerDocumentCategory.DropDownList());

                ControllerDocument controllerDocument = new ControllerDocument(db);
                var document = controllerDocument.Cari(Request.QueryString["id"].ToInt());

                if (document != null)
                {
                    listCompany.SelectedValue = document.IDCompany.ToString();
                    listCategory.SelectedValue = document.IDCategory.ToString();
                    InputDescription.Text = document.Description;


                    btnOk.Text = "Update";
                    LabelTitle.Text = "Update Document";
                }
                else
                {
                    btnOk.Text = "Add New";
                    LabelTitle.Text = "Add New Document";
                }
            }
        }
    }

    protected void btnOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerDocument controllerDocument = new ControllerDocument(db);

                if (btnOk.Text == "Add New")
                {
                    controllerDocument.Create(int.Parse(listCompany.SelectedValue), int.Parse(listCategory.SelectedValue), InputName.Text,
                        InputDescription.Text);
                }
                else if (btnOk.Text == "Update")
                    controllerDocument.Update(Request.QueryString["id"].ToInt(), int.Parse(listCompany.SelectedValue), int.Parse(listCategory.SelectedValue),
                        InputName.Text,
                        InputDescription.Text);
                db.SubmitChanges();

                Response.Redirect("/Document/Default.aspx");
            }
        }
    }
}