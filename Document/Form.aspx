﻿<%@ Page Title="" Language="C#" MasterPageFile="~/assets/MasterPage.master" AutoEventWireup="true" CodeFile="Form.aspx.cs" Inherits="Document_Form" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderTitle" runat="Server">
    <asp:Label ID="LabelTitle" runat="server"></asp:Label>
    <hr />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label>Company</label>
                        <asp:DropDownList ID="listCompany" CssClass="form-control" runat="server" Width="100%"></asp:DropDownList>
                    </div>
                    <div class="col-md-6">
                        <label>Category</label>
                        <asp:DropDownList ID="listCategory" CssClass="form-control" runat="server" Width="100%"></asp:DropDownList>
                    </div>
                    <div class="col-md-6">
                        <label>Name</label>
                        <asp:TextBox ID="InputName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Description</label>
                <asp:TextBox ID="InputDescription" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
            </div>
            <asp:Button ID="btnOk" CssClass="btn btn-success btn-sm" runat="server" Text="Add New" OnClick="btnOk_Click" />
            <a href="Default.aspx" class="btn btn-danger btn-sm">Cancel</a>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderJavascript" runat="Server">
</asp:Content>

